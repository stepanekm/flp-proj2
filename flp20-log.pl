/** flp20-log.pl Hamiltonian cycle
author: Martin Štěpánek (xstepa59)
*/

%Checks if input is valid name of vertex (A-Z).
is_valid_vertex_name(V) :- char_code(V, Code), Code > 64, Code < 91 .


%Parses input line. If it is valid edge, it inserts the edge into the database.
parse_input_line([V1, ' ', V2]) :-
        is_valid_vertex_name(V1),
        is_valid_vertex_name(V2),
        asserta(edge_to(V1, V2)).

parse_input_line(_).


%Parses input lines.
parse_input_lines([]).
parse_input_lines([L|Ls]) :- parse_input_line(L), parse_input_lines(Ls).


%Checks if there is edge between two vertices in the database.
edge(V1, V2) :- edge_to(V1, V2), V1 \= V2.
edge(V1, V2) :- edge_to(V2, V1), V1 \= V2.


%Make sure, that `edge_to` predicate always exists,
%even when the input did not contain any valid edge.
:- dynamic edge_to/2.
edge_to(_, _) :- fail.


%Gets all vertices (sorted).
get_vertices(Vertices) :- findall(Vertex, edge(Vertex, _), Vertices_list),
                          sort(Vertices_list, Vertices).


%Reverses Hamiltonian cycle.
%E.g. transforms [[A,B], [B,C], [C,A]] into [[A,C], [C,B], [B,A]].
reverse_hamilton([], []).
reverse_hamilton([[V1, V2]|Hamilton], Reversed) :-
        reverse_hamilton(Hamilton, Reversed_tmp),
        append(Reversed_tmp, [[V2, V1]], Reversed).


%Finds one Hamiltonian cycle given the first vertex in the cycle.
find_one_hamilton_aux([], First, [[Last_but_one, Last], [Last, First]]) :-
        edge(Last, First),
        Last_but_one \= First.

find_one_hamilton_aux([First|Vertices], First, [[First, Second]|Edges]) :-
        member(Second, Vertices),
        edge(First, Second),
        ord_subtract(Vertices, [Second], Remaining_vertices),
        find_one_hamilton_aux(Remaining_vertices, First, [[First, Second]|Edges]).

find_one_hamilton_aux(Vertices, First, [[_, V2], [V2, V3]|Edges]) :-
        not(member(First, Vertices)),
        member(V3, Vertices),
        edge(V2, V3),
        ord_subtract(Vertices, [V3], Remaining_vertices),
        find_one_hamilton_aux(Remaining_vertices, First, [[V2, V3]|Edges]).


%Finds one Hamiltonian cycle.
find_one_hamilton([First|Vertices], Hamilton) :-
        find_one_hamilton_aux([First|Vertices], First, Hamilton).


%Removes duplicate Hamiltonian cycles.
remove_duplicates([], []).
remove_duplicates([H|Hs], [H|Unique_hamiltons]) :-
        reverse_hamilton(H, Reversed_H),
        subtract(Hs, [Reversed_H], Tmp),
        remove_duplicates(Tmp, Unique_hamiltons).


%Finds all Hamiltonian cycles.
find_all_hamiltons(Vertices, Hamiltons) :-
        findall(Hamilton, find_one_hamilton(Vertices, Hamilton), Hamiltons_with_duplicates),
        remove_duplicates(Hamiltons_with_duplicates, Hamiltons).


%Transforms Hamiltonian cycle into output format.
one_to_output_format([], []).
one_to_output_format([Edge|Edges], [Edge_formated|Edges_formated]) :-
        atomic_list_concat(Edge, '-', Edge_formated),
        one_to_output_format(Edges, Edges_formated).


%Transforms Hamiltonian cycles into output format.
all_to_output_format([], []).
all_to_output_format([H|Hs], [H_formated|Hs_formated]) :-
        one_to_output_format(H, H_formated_tmp),
        atomic_list_concat(H_formated_tmp, ' ', H_formated),
        all_to_output_format(Hs, Hs_formated).


main :-
        prompt(_, ''),
        read_lines(Input),
        parse_input_lines(Input),
        get_vertices(Vertices),
        find_all_hamiltons(Vertices, Hamiltons),
        all_to_output_format(Hamiltons, Hamiltons_formated),
        write_lines2(Hamiltons_formated),
        halt.


/** FLP 2020
Toto je ukazkovy soubor zpracovani vstupu v prologu.
autor: Martin Hyrs, ihyrs@fit.vutbr.cz
*/
read_line(L,C) :-
	get_char(C),
	(isEOFEOL(C), L = [], !;
		read_line(LL,_),% atom_codes(C,[Cd]),
		[C|LL] = L).


isEOFEOL(C) :-
	C == end_of_file;
	(char_code(C,Code), Code==10).


read_lines(Ls) :-
	read_line(L,C),
	( C == end_of_file, Ls = [] ;
	  read_lines(LLs), Ls = [L|LLs]
	).

write_lines2([]).
write_lines2([H|T]) :- writeln(H), write_lines2(T).
