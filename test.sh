# flp20-log test.sh
# author: Martin Štěpánek (xstepa59)

inputs=($(find tests -name "*.in"))

for input in "${inputs[@]}"
do
    ref_out=$(echo "${input%%.*}")".out"
    out="$(./flp20-log <$input 2>&1; echo x)"
    out=${out%x}
    diff_out=$(diff <(printf "$out") "$ref_out")
    if [ -z "$diff_out" ]
    then
        printf "$input OK\n"
    else
        printf "$input Fail: $diff_out \n"
    fi
done
