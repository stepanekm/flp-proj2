Project: flp20-log
Assignment: Hamiltonian cycle
Author: Martin Štěpánek (xstepa59)

The project contains tests, which can be run by running `./test.sh`.
Running time of all tests is less than 1 second.
